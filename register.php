<?php
$genderArray = array(0 => "Nam", 1 => "Nữ");
$departmentArray = array("EMPTY" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="register.css">


</head>

<body>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>

    <script type="text/javascript">
        $(function() {
            $("#datepicker").datepicker({
                dateFormat: "dd/mm/yy"
            }).val()
        });
    </script>

    
    
    <div class='login-content'>
        <div id = "label-infor">
            <label class="label">
                <?php
                    if(isset($_POST['submit'])){
                        if(empty($_POST["studentName"])){
                            echo "Hãy nhập tên.";
                            echo "<br>";  
                        }
                        if(empty($_POST["gender"])){
                            echo "Hãy chọn giới tính.";
                            echo "<br>";  
                        }
                        if(empty($_POST["department"])){
                            echo "Hãy chọn phân khoa.";
                            echo "<br>"; 
                        }
                        if(empty($_POST["birthday"])){
                            echo "Hãy nhập ngày sinh.";
                        }else{
                            if (!preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/",$_POST["birthday"])) {
                                echo "Hãy nhập ngày sinh
                                đúng định dạng.";
                            } 
                        }
                    }
                ?>
            </label>
        </div>
        <form class='form-input' method="post" action="<?php echo $_SERVER["PHP_SELF"];?>">
            <div class="content">
                <div class='lb'>
                    <label class="label-star">Họ và tên </label>
                </div>
                <input type='text' class='input-item' name="studentName">
            </div>
            <div class="content">
                <div class="lb">
                    <label class="label-star">Giới tính</label>
                </div>
                <div id="radio-content">
                    <?php
                    for ($x = 0; $x < count($genderArray); $x++) {
                        echo ("
                             <input type=\"radio\"  name=\"gender\" >$genderArray[$x]</input>
                        ");
                    }

                    ?>
                </div>

            </div>
            <div class="content">
                <div class="lb">
                    <label class="label-star">Phân khoa</label>
                </div>
                <select name="department" class="select-item">
                    <?php
                    
                    foreach ($departmentArray as $value) {
                        echo ("
                            <option name=\"department\">$value</option>
                        ");
                    }
                    ?>
                </select>
            </div>
            <div class="content">
                <div class='lb'>
                    <label class="label-star">Ngày sinh</label>
                </div>
                <input type='text' name="birthday" class='input-address' placeholder="dd/mm/yyyy" id="datepicker">

            </div>
            <div class="content">
                <div class='lb'>
                    <label>Địa chỉ </label>
                </div>
                <input type='text' class='input-item' name="address">
            </div>

            <input type='submit' value='Đăng ký' id='btn-submit' name="submit">
        </form>


    </div>


</body>

</html>